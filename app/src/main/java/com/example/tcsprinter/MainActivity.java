package com.example.tcsprinter;

import android.bluetooth.BluetoothAdapter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.tscdll.TSCActivity;


public class MainActivity extends Activity {

    TSCActivity TscDll = new TSCActivity();
    private static final int REQUEST_WRITE_PERMISSION = 786;

    private Button test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermission();

        test = (Button) findViewById(R.id.button1);

        test.setOnClickListener(new OnClickListener() {
            public void onClick(View v)
            {

                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {
                    // Device does not support Bluetooth
                } else if (!mBluetoothAdapter.isEnabled()) {
                    Toast.makeText(MainActivity.this,"Please turn your bluetooth On.",Toast.LENGTH_SHORT).show();
                } else {
                    TscDll.openport("00:19:0E:A3:B5:CB");

                    //String status = TscDll.printerstatus(300);

                    TscDll.sendcommand("SIZE 85 mm, 100 mm\r\n");
                  //  TscDll.sendcommand("GAP 2 mm, 0 mm\r\n");//Gap media
                    TscDll.sendcommand("BLINE 2 mm, 0 mm\r\n");//blackmark media
                    TscDll.clearbuffer();
                    TscDll.sendcommand("SPEED 4\r\n");
                    TscDll.sendcommand("DENSITY 12\r\n");
                    TscDll.sendcommand("CODEPAGE UTF-8\r\n");
                    TscDll.sendcommand("SET TEAR ON\r\n");
                   /* TscDll.sendcommand("SET COUNTER @1 1\r\n");
                    TscDll.sendcommand("@1 = \"0001\"\r\n");
                    TscDll.sendcommand("TEXT 100,300,\"ROMAN.TTF\",0,12,12,@1\r\n");
                    TscDll.sendcommand("TEXT 100,400,\"ROMAN.TTF\",0,12,12,\"TEST FONT\"\r\n");*/
                    TscDll.qrcode(160,120,"H","10","A","0","M1","S7","123456789");

                    //TscDll.barcode(100, 100, "128", 100, 1, 0, 3, 3, "123456789");
                    TscDll.printerfont(0, 0, "4", 0, 1, 1, "Testing Content");
                    TscDll.printerfont(100, 400, "3", 0, 1, 1, "Testing Content");
                    TscDll.printerfont(100, 430, "3", 0, 1, 1, "Testing Content");
                    TscDll.printerfont(100, 460, "3", 0, 1, 1, "Testing Content");
                    TscDll.printlabel(2, 1);

                    TscDll.closeport(5000);
                }

            }

        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
           // openFilePicker();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
           // openFilePicker();
        }
    }

}

